import os
import pandas as pd
import datetime as d
import random as r
global counter


def DataCreation():
    global counter
    fileName = 'test_' + str(counter) + '.csv'
    # if counter == 100:
    #     counter = 0
    # else:
    #     counter += 1
    counter += 1
    cars = {'Brand': ['Honda Civic', 'Toyota Corolla', 'Ford Focus', 'Audi A4'],
            'Price': [r.randrange(5000,100000), r.randrange(5000,50000), r.randrange(10000,200000), r.randrange(500000,1000000)],
            'Date': [d.date.today(), d.date.today(), d.date.today(), d.date.today()],
            'Time': [d.datetime.now().time(), d.datetime.now().time(), d.datetime.now().time(), d.datetime.now().time()]
            }

    df = pd.DataFrame(cars, columns=['Date', 'Time', 'Brand', 'Price'])
    df.to_csv(os.getcwd() + "\\" + fileName, index=False, header=True)
    print("File Created: " + str(counter))



def main():
    global counter
    counter = 0

    previous = d.datetime.now().minute
    if previous == 60:
        previous = 0

    while True:
        current = d.datetime.now().minute
        if current == 0:
            previous = -1

        if current > previous:
            previous = current
            DataCreation()

if __name__ == "__main__":
    main()
